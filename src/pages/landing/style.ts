import styled from 'styled-components'

export const LandingPageWrapper = styled.div`
  background-color: rgba(255, 255, 255, 0.3);
  max-width: 500px;
  margin: auto;

  .MuiInputBase-root {
    margin-bottom: 8px;
  }
`

export const ValueDisplayWrapper = styled.div`
  max-width: 300px;
  width: 100%;
  height: 60px;
  border: 2px solid rgba(0, 0, 0, 0.5);
  border-radius: 4px;
  margin: auto;
  margin-top: 24px;
  font-size: 1em;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
`
