import React, { ChangeEvent, useEffect, useState } from 'react'
import TextField from '@material-ui/core/TextField'
import { MenuItem, Select } from '@material-ui/core'
import { MaritalStatus } from 'enums'
import { calculate } from 'utils'
import { LandingPageWrapper, ValueDisplayWrapper } from './style'

const LandingPage = (): JSX.Element => {
  const [values, setValues] = useState({
    annualSalary: 0,
    maritalStatus: 0,
    children: 0
  })
  const [result, setResult] = useState(0)

  const handleSelectChange = (evt: ChangeEvent<HTMLSelectElement>) => {
    const field = evt.target.name
    setValues({
      ...values,
      [field]: evt.target.value
    })
  }

  const handleChange = (evt: ChangeEvent<HTMLInputElement>) =>
    setValues({
      ...values,
      [evt.target.name]: parseInt(evt.target.value, 10)
    })

  const getProperType = (): MaritalStatus => {
    switch (values.children) {
      case 0:
        return MaritalStatus.Married0Kids
      case 1:
        return MaritalStatus.Married1Kids
      case 2:
        return MaritalStatus.Married2Kids
      case 3:
        return MaritalStatus.Married3Kids
      default:
        return MaritalStatus.Married0Kids
    }
  }

  useEffect(() => {
    if (values.annualSalary !== 0) {
      if (values.maritalStatus === MaritalStatus.Single) {
        setResult(calculate(values.annualSalary, MaritalStatus.Single))
      } else {
        const type = getProperType()
        setResult(calculate(values.annualSalary, type))
      }
    }
  }, [values])

  return (
    <LandingPageWrapper>
      <div>Indonesia Personal Income Tax Calculator</div>
      <TextField
        name="annualSalary"
        onChange={handleChange}
        value={values.annualSalary}
        type="number"
        label="Annual Salary"
        fullWidth
      />
      <Select
        name="maritalStatus"
        onChange={handleSelectChange}
        value={values.maritalStatus}
        fullWidth
      >
        <MenuItem value={0}>Single</MenuItem>
        <MenuItem value={1}>Married</MenuItem>
      </Select>
      <TextField
        name="children"
        onChange={handleChange}
        value={values.children}
        type="number"
        label="Children"
        fullWidth
      />
      <ValueDisplayWrapper>Tax: {result}</ValueDisplayWrapper>
    </LandingPageWrapper>
  )
}

export default LandingPage
