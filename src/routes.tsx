import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import LandingPage from 'pages/landing'

const App = (): JSX.Element => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" render={() => <LandingPage />} />
      </Switch>
    </Router>
  )
}

export default App
