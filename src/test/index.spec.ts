import { expect } from 'chai'
import { calculate } from '../utils'
import { MaritalStatus } from '../enums'

describe('tax', () => {
  it('should compute for single', () => {
    const actual = calculate(300000000, MaritalStatus.Single)

    expect(actual).to.be.eq(31900000)
  })
  it('should compute for married with 1 dependents', () => {
    const actual = calculate(78000000, MaritalStatus.Married1Kids)

    expect(actual).to.be.eq(750000)
  })
})
