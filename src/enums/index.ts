export enum MaritalStatus {
  Single = 0,
  Married0Kids = 1,
  Married1Kids = 2,
  Married2Kids = 3,
  Married3Kids = 4
}
