import { MaritalStatus } from 'enums'

const million = 1000000

const getDeductibles = (type: MaritalStatus): number => {
  switch (type) {
    case MaritalStatus.Single:
      return 54 * million
    case MaritalStatus.Married0Kids:
      return 58.5 * million
    case MaritalStatus.Married1Kids:
      return 63 * million
    case MaritalStatus.Married2Kids:
      return 67.5 * million
    case MaritalStatus.Married3Kids:
      return 72 * million
    default:
      return 0
  }
}

export const calculate = (
  annualSalary: number,
  type: MaritalStatus
): number => {
  const taxable = annualSalary - getDeductibles(type)
  let tax = 0.05 * (taxable < 50 * million ? taxable : 50 * million)

  if (taxable >= 50 * million) {
    let value = taxable - 50 * million

    if (taxable >= 250 * million) {
      value = 250 * million
    }

    tax += value * 0.15
  }

  if (taxable >= 250 * million) {
    let value = taxable - 250 * million

    if (taxable >= 500 * million) {
      value = 500 * million
    }

    tax += value * 0.25
  }

  if (taxable >= 500 * million) {
    const value = taxable - 500 * million

    tax += value * 0.3
  }

  return tax
}
