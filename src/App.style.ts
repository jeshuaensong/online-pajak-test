import styled from 'styled-components'

export const Wrapper = styled.div`
  background: #f8faff;
  width: 100%;
  height: 100%;
`

export default Wrapper
